<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CompanyResponsibleUser extends Model {
    protected $table = 'companyresponsibleuser';
    protected $fillable = ['companyId', 'userId'];
}