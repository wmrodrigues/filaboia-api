<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class BranchResponsibleUser extends Model {
    protected $table = 'branchresponsibleuser';
    protected $fillable = ['companyId', 'branchId', 'userId'];
}