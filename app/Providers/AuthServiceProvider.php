<?php

namespace App\Providers;

use App\User;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Crypt;
use App\Http\Helpers\TokenData as TokenData;
use Illuminate\Contracts\Encryption\DecryptException;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Boot the authentication services for the application.
     *
     * @return void
     */
    public function boot()
    {
        // Here you may define how you wish users to be authenticated for your Lumen
        // application. The callback which receives the incoming request instance
        // should return either a User instance or null. You're free to obtain
        // the User instance via an API token or any other method necessary.

        $this->app['auth']->viaRequest('api', function ($request) {
            try {
                $token = $request->header('token');
                if (!empty($token)) {
                    $token = Crypt::decrypt($token);
                    $values = explode('|', $token);
                    $date = $values[TokenData::DATETIME];
                    $now = new \DateTime();
                    $expires = new \DateTime($date);
                    return $now <= $expires;
                }
            } catch(DecryptException $e) {
                //log some dummy error
            } catch(Exception $e) {
                //log some error
            }
            return null;
        });
    }
}
