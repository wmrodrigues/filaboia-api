<?php
namespace App\Repositories;

use App\Repositories\Contracts\RepositoryInterface;
use App\Repositories\Eloquent\Repository;

class UserRepository extends Repository {
    function model() {
        return 'App\Models\User';
    }

    public function getByLoginPassword($login, $password) {
        return $this->model->where([
                                ['login', '=', $login],
                                ['password', '=', $password]
                            ])->first();
    }

    public function getByToken($token) {
        return $this->model->where('password', '=', $token)
                            ->first();
    }

    public function getByLogin($login) {
        return $this->model->where('login', '=', $login)
                            ->first();
    }

    public function getByDocument($document) {
        return $this->model->where('document', '=', $document)
                            ->first();
    }

    public function getResponsibleUsersByCompanyId($companyId) {
        $data = $this->model->join('companyresponsibleuser', 'users.id', '=', 'companyresponsibleuser.userId')
                            ->where('companyresponsibleuser.companyId', '=', $companyId)
                            ->select('users.*')
                            ->get();
        return $data;
    }

    public function getResponsibleUsersByBranchId($branchId) {
        $data = $this->model->join('branchresponsibleuser', 'users.id', '=', 'branchresponsibleuser.userId')
                            ->where('branchresponsibleuser.branchId', '=', $branchId)
                            ->select('users.*')
                            ->get();
        return $data;
    }
}