<?php
namespace App\Repositories;

use App\Repositories\Contracts\RepositoryInterface;
use App\Repositories\Eloquent\Repository;

class CompanyResponsibleUserRepository extends Repository {
    function model() {
        return 'App\Models\CompanyResponsibleUser';
    }

    public function getByCompanyIdUserId($companyId, $userId) {
        return $this->model->where([
                                    ['companyId', '=', $companyId],
                                    ['userId', '=', $userId]
                            ])->first();
    }
}