<?php
namespace App\Repositories;

use App\Repositories\Contracts\RepositoryInterface;
use App\Repositories\Eloquent\Repository;

class CompanyRepository extends Repository {
    function model() {
        return 'App\Models\Company';
    }

    public function getByDocument($document) {
        return $this->model->where('document', '=', $document)
                            ->first();
    }
}