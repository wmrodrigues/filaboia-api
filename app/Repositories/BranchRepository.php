<?php
namespace App\Repositories;

use App\Repositories\Contracts\RepositoryInterface;
use App\Repositories\Eloquent\Repository;

class BranchRepository extends Repository {
    function model() {
        return 'App\Models\Branch';
    }

    public function getByDocument($document) {
        return $this->model->where('document', '=', $document)
                            ->first();
    }

    public function getByCompanyIdName($companyId, $name) {
        return $this->model->where([
                                    ['companyid', '=', $companyId],
                                    ['name', '=', $name]
                                    ])->first();
    }

    public function getBranchCountByCompanyId($companyId) {
        return $this->model->where('companyId', '=', $companyId)->count();
    }

    public function getByCompanyId($companyId) {
        return $this->model->where('companyid', '=', $companyId)
                            ->get();
    }
}