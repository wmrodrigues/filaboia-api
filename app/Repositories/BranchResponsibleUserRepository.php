<?php
namespace App\Repositories;

use App\Repositories\Contracts\RepositoryInterface;
use App\Repositories\Eloquent\Repository;

class BranchResponsibleUserRepository extends Repository {
    function model() {
        return 'App\Models\BranchResponsibleUser';
    }

    public function getByCompanyIdBranchIdUserId($companyId, $branchId, $userId) {
        return $this->model->where([
                                    ['companyId', '=', $companyId],
                                    ['branchId', '=', $branchId],
                                    ['userId', '=', $userId]
                            ])->first();
    }

    public function getByBranchIdUserId($branchId, $userId) {
        return $this->model->where([
                                    ['branchId', '=', $branchId],
                                    ['userId', '=', $userId]
                            ])->first();
    }
}