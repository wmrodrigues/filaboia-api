<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Business\BranchBusiness as BranchBusiness;
use App\Models\Branch as Branch;
use App\Exceptions\EntityValidationException;
use Illuminate\Support\Collection;

class BranchsController extends Controller {

    private $branchBusiness;
    private $request;
    private $logger;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(Request $request, BranchBusiness $bb) {
        $this->request = $request;
        $this->branchBusiness = $bb;
    }

    public function get() {
        $pager = $this->getPager($this->request);

        $data = $this->branchBusiness->paginate($pager);
        if (empty($data->total()))
            return $this->createEmptyResponse();
        return $this->createPagedResponse($data);
    }

    public function getById($id) {
        $result = $this->branchBusiness->getById($id);
        if (empty($result))
            return $this->createEmptyResponse();

        return $this->createDefaultResponse($result);
    }

    public function post(Request $request) {
        try {
            $data = $request->all();
            if (empty($data))
                return $this->createBadRequestResponse();
            
            $entity = new Branch();
            $entity->companyId = $data['companyId'];
            $entity->document = $data['document'];
            $entity->name = $data['name'];

            $this->branchBusiness->save($entity);

            return $this->createCreatedResponse($entity->id);
        } catch(EntityValidationException $e) {
            return $this->createPreConditionFailedResponse($e->getMessage());
        } catch(Exception $e) {
            return $this->createInternalServerErrorResponse($e->getMessage());
        }
    }

    public function put(Request $request, $id) {
        try {
            $data = $request->all();
            if (empty($data))
                return $this->createBadRequestResponse();
            
            if (empty($data['id']) || $data['id'] != $id)
                return $this->createModelBadRequestResponse();

            $entity = $this->branchBusiness->getById($id);
            if (empty($entity))
                return $this->createEmptyResponse();
            
            $entity->id = $data['id'];
            $entity->companyId = $data['companyId'];
            $entity->document = $data['document'];
            $entity->name = $data['name'];

            $this->branchBusiness->save($entity);

            return $this->createDefaultResponse();
        } catch(EntityValidationException $e) {
            return $this->createPreConditionFailedResponse($e->getMessage());
        } catch(Exception $e) {
            return $this->createInternalServerErrorResponse($e->getMessage());
        }
    }

    public function delete($id) {
        try {
            $entity = $this->branchBusiness->getById($id);
            if (empty($entity))
                return $this->createEmptyResponse();

            $this->branchBusiness->delete($entity->id);
            return $this->createDefaultResponse();            
        } catch(Exception $e) {
            return $this->createInternalServerErrorResponse($e->getMessage());
        }
    }

    public function getByCompanyId($companyId) {
        try {
            $result = $this->branchBusiness->getByCompanyId($companyId);
            if (empty($result))
                return $this->createEmptyResponse();

            return $this->createDefaultResponse($result);
        } catch(EntityValidationException $e) {
            return $this->createPreConditionFailedResponse($e->getMessage());
        } catch(Exception $e) {
            return $this->createInternalServerErrorResponse($e->getMessage());
        }
    }
}
