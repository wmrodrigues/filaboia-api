<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Business\UserBusiness as UserBusiness;
use App\Models\User as User;
use App\Exceptions\EntityValidationException;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Crypt;
use App\Http\Helpers;

class AccountsController extends Controller {
    private $userBusiness;
    private $request;
    private $logger;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(Request $request, UserBusiness $ub) {
        $this->request = $request;
        $this->userBusiness = $ub;
    }

    public function postLoginToken(Request $request) {
        try {
            $data = $request->all();
            if (empty($data))
                return $this->createBadRequestResponse();
            
            //$this->checkObjectProperties($data, collect(['login', 'password']));
            $data = $this->userBusiness->getByLoginPassword($data['login'], $data['password']);
            if (empty($data))
                return $this->createEmptyResponse();
            
            unset($data->token);
            return $this->createDefaultResponse($data);

        } catch(EntityValidationException $e) {            
            return $this->createPreConditionFailedResponse($e->getMessage());
        } catch(Exception $e) {
            return $this->createInternalServerErrorResponse($e->getMessage());
        }
    }

    public function postToken(Request $request) {
        try {
            $data = $request->all();
            if (empty($data))
                return $this->createBadRequestResponse();

            $data = $this->userBusiness->getByToken($data['token']);
            if (empty($data))
                return $this->createEmptyResponse();
            
            unset($data->token);

            return $this->createDefaultResponse($data);

        } catch(EntityValidationException $e) {
            return $this->createPreConditionFailedResponse($e->getMessage());
        } catch(Exception $e) {
            return $this->createInternalServerErrorResponse($e->getMessage());
        }
    }

    public function requestToken(Request $request) {
        try {
            $expires = env('TOKEN_EXPIRES_SECONDS');
            $username = $request->header('username');
            $password = $request->header('password');
            $date = new \DateTime();
            $date->modify("+$expires seconds");

            //search on database for the user entity
            $token = new \stdClass();
            $token->access_token = Crypt::encrypt($username . '|' . $date->format('Y-m-d H:i:s'));
            $token->expires_in = $expires;
            $token->date = $date->format('Y-m-d H:i:s');

            return $this->createDefaultResponse($token);

        } catch(Exception $e) {
            return $this->createInternalServerErrorResponse($e->getMessage());
        }
    }

    public function validateToken(Request $request) {
        try {
            $token = $request->header('token');

            $token = Crypt::decrypt($token);
            $values = explode('|', $token);

            // echo $values[TokenData::USERNAME];
            $date = $values[TokenData::DATETIME];
            $now = new \DateTime();
            $expires = new \DateTime($date);
            if ($now < $expires) {
                echo 'Authorized!';
                print_r($now);
                print_r($expires);
            }

            return $this->createDefaultResponse($values);

        } catch(Exception $e) {
            return $this->createInternalServerErrorResponse($e->getMessage());
        }
    }
}