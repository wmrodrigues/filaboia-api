<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Business\CompanyBusiness as CompanyBusiness;
use App\Models\Company as Company;
use App\Exceptions\EntityValidationException;
use Illuminate\Support\Collection;

class CompanysController extends Controller {

    private $companyBusiness;
    private $request;
    private $logger;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(Request $request, CompanyBusiness $cb) {
        $this->request = $request;
        $this->companyBusiness = $cb;
    }

    public function get() {
        $pager = $this->getPager($this->request);

        $data = $this->companyBusiness->paginate($pager);
        if (empty($data->total()))
            return $this->createEmptyResponse();
        return $this->createPagedResponse($data);
    }

    public function getById($id) {
        $result = $this->companyBusiness->getById($id);
        if (empty($result))
            return $this->createEmptyResponse();

        return $this->createDefaultResponse($result);
    }

    public function post(Request $request) {
        try {
            $data = $request->all();
            if (empty($data))
                return $this->createBadRequestResponse();
            
            $entity = new Company();
            $entity->fantasyName = $data['fantasyName'];
            $entity->officialName = $data['officialName'];
            $entity->document = $data['document'];

            $this->companyBusiness->save($entity);

            return $this->createCreatedResponse($entity->id);
        } catch(EntityValidationException $e) {
            return $this->createPreConditionFailedResponse($e->getMessage());
        } catch(Exception $e) {
            return $this->createInternalServerErrorResponse($e->getMessage());
        }
    }

    public function put(Request $request, $id) {
        try {
            $data = $request->all();
            if (empty($data))
                return $this->createBadRequestResponse();
            
            if (empty($data['id']) || $data['id'] != $id)
                return $this->createModelBadRequestResponse();

            $entity = $this->companyBusiness->getById($id);
            if (empty($entity))
                return $this->createEmptyResponse();
            
            $entity->id = $data['id'];
            $entity->fantasyName = $data['fantasyName'];
            $entity->officialName = $data['officialName'];
            $entity->document = $data['document'];

            $this->companyBusiness->save($entity);

            return $this->createDefaultResponse();
        } catch(EntityValidationException $e) {
            return $this->createPreConditionFailedResponse($e->getMessage());
        } catch(Exception $e) {
            return $this->createInternalServerErrorResponse($e->getMessage());
        }
    }

    public function delete($id) {
        try {
            $entity = $this->companyBusiness->getById($id);
            if (empty($entity))
                return $this->createEmptyResponse();

            $this->companyBusiness->delete($entity->id);
            return $this->createDefaultResponse();            
        } catch(Exception $e) {
            return $this->createInternalServerErrorResponse($e->getMessage());
        }
    }
}
