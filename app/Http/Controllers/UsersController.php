<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Business\UserBusiness as UserBusiness;
use App\Models\User as User;
use App\Exceptions\EntityValidationException;
use Illuminate\Support\Collection;
use App\Business\CompanyBusiness as CompanyBusiness;
use App\Business\BranchBusiness as BranchBusiness;

class UsersController extends Controller {
    private $userBusiness;
    private $companyBusiness;
    private $branchBusiness;
    private $companyResponsibleUserBusiness;
    private $request;
    private $logger;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(Request $request, UserBusiness $ub, CompanyBusiness $cb, BranchBusiness $bb) {
        $this->request = $request;
        $this->userBusiness = $ub;
        $this->companyBusiness = $cb;
        $this->branchBusiness = $bb;
    }

    public function get() {
        $pager = $this->getPager($this->request);

        $data = $this->userBusiness->paginate($pager);
        if (empty($data->total()))
            return $this->createEmptyResponse();
        return $this->createPagedResponse($data);
    }

    public function getById($id) {
        $result = $this->userBusiness->getById($id);
        if (empty($result))
            return $this->createEmptyResponse();

        unset($result->password);
        return $this->createDefaultResponse($result);
    }

    public function post(Request $request) {
        try {
            $data = $request->all();
            if (empty($data))
                return $this->createBadRequestResponse();
            
            $entity = new User();
            $entity->name = $data['name'];
            $entity->login = $data['login'];
            $entity->password = $data['password'];
            $entity->document = $data['document'];
            $entity->birthDate = $data['birthDate'];
            $entity->phone = $data['phone'];
            $entity->email = $data['email'];

            $this->userBusiness->save($entity);

            return $this->createCreatedResponse($entity->id);
        } catch(EntityValidationException $e) {
            return $this->createPreConditionFailedResponse($e->getMessage());
        } catch(Exception $e) {
            return $this->createInternalServerErrorResponse($e->getMessage());
        }
    }

    public function put(Request $request, $id) {
        try {
            $data = $request->all();
            if (empty($data))
                return $this->createBadRequestResponse();
            
            if (empty($data['id']) || $data['id'] != $id)
                return $this->createModelBadRequestResponse();

            $entity = $this->userBusiness->getById($id);
            if (empty($entity))
                return $this->createEmptyResponse();
            
            $entity->id = $data['id'];
            $entity->name = $data['name'];
            $entity->login = $data['login'];
            $entity->password = $data['password'];
            $entity->document = $data['document'];
            $entity->birthDate = $data['birthDate'];
            $entity->phone = $data['phone'];
            $entity->email = $data['email'];

            $this->userBusiness->save($entity);

            return $this->createDefaultResponse();
        } catch(EntityValidationException $e) {
            return $this->createPreConditionFailedResponse($e->getMessage());
        } catch(Exception $e) {
            return $this->createInternalServerErrorResponse($e->getMessage());
        }
    }

    public function delete($id) {
        try {
            $entity = $this->userBusiness->getById($id);
            if (empty($entity))
                return $this->createEmptyResponse();

            $this->userBusiness->delete($entity->id);
            return $this->createDefaultResponse();            
        } catch(Exception $e) {
            return $this->createInternalServerErrorResponse($e->getMessage());
        }
    }

    public function postCompanyResponsibleUsers(Request $request, $id) {
        try {
            $users = $request->all();
            $usersNotFound = array();
            $this->companyBusiness->addResponsibleUsers($id, $users, $usersNotFound);

            return $this->createDefaultResponse($usersNotFound);
        } catch(EntityValidationException $e) {
            return $this->createPreConditionFailedResponse($e->getMessage());
        } catch(Exception $e) {
            return $this->createInternalServerErrorResponse($e->getMessage());
        }
    }

    public function deleteCompanyResponsibleUsers(Request $request, $id) {
        try {
            $users = $request->all();
            $this->companyBusiness->removeResponsibleUsers($id, $users);

            return $this->createDefaultResponse();
        } catch(EntityValidationException $e) {
            return $this->createPreConditionFailedResponse($e->getMessage());
        } catch(Exception $e) {
            return $this->createInternalServerErrorResponse($e->getMessage());
        }
    }

    public function postBranchResponsibleUsers(Request $request, $id) {
        try {
            $users = $request->all();
            $usersNotFound = array();
            $this->branchBusiness->addResponsibleUsers($id, $users, $usersNotFound);

            return $this->createDefaultResponse();
        } catch(EntityValidationException $e) {
            return $this->createPreConditionFailedResponse($e->getMessage());
        } catch(Exception $e) {
            return $this->createInternalServerErrorResponse($e->getMessage());
        }
    }

    public function deleteBranchResponsibleUsers(Request $request, $id) {
        try {
            $users = $request->all();
            $this->branchBusiness->removeResponsibleUsers($id, $users);

            return $this->createDefaultResponse();
        } catch(EntityValidationException $e) {
            return $this->createPreConditionFailedResponse($e->getMessage());
        } catch(Exception $e) {
            return $this->createInternalServerErrorResponse($e->getMessage());
        }
    }

    public function getResponsibleUsersByCompanyId($companyId) {
        try {
            $users = $this->userBusiness->getResponsibleUsersByCompanyId($companyId);
            if (empty($users))
                return $this->createEmptyResponse();
            
            return $this->createDefaultResponse($users);
        } catch(EntityValidationException $e) {
            return $this->createPreConditionFailedResponse($e->getMessage());
        } catch(Exception $e) {
            return $this->createInternalServerErrorResponse($e->getMessage());
        }
    }

    public function getResponsibleUsersByBranchId($branchId) {
        try {
            $users = $this->userBusiness->getResponsibleUsersByBranchId($branchId);
            if (empty($users))
                return $this->createEmptyResponse();
            
            return $this->createDefaultResponse($users);
        } catch(EntityValidationException $e) {
            return $this->createPreConditionFailedResponse($e->getMessage());
        } catch(Exception $e) {
            return $this->createInternalServerErrorResponse($e->getMessage());
        }
    }
}