<?php

namespace App\Http\Controllers;

use Illuminate\Http\Response;
use Illuminate\Http\Request;
use Laravel\Lumen\Routing\Controller as BaseController;
use App\Http\Helpers\Pager;
use Illuminate\Routing\UrlGenerator;
use App\Exceptions\EntityValidationException;

class Controller extends BaseController {
    public function getPager($request) {
        $pager = new Pager();
        
        $pager->pageSize = !$request->hasHeader('pageSize') ? 10 : $request->header('pageSize');
        $pager->currentPage = !$request->hasHeader('currentPage') ? 0 : $request->header('currentPage');

        $pager->currentPage++;

        return $pager;
    }

    public function checkObjectProperties($array, $keys) {
        $keys->each(function($item, $key) use($array) {
            if (!array_key_exists($item, $array))
                throw new EntityValidationException("$item is not present on object");
        });

        return true;
    } 

    public function getUserImageUri($id) {
        return url() . "/api/v1/users/$id/image";
    }

    public function getUtensilImageUri($id) {
        return url() . "/api/v1/utensils/$id/image";
    }

    public function getRecipeImageUri($id) {
        return url() . "/api/v1/recipes/$id/image";
    }

    protected function encodeImageToBase64($request) {
        $image = file_get_contents($request->file('image'));
        $base64 = base64_encode($image);
        return $base64;
    }

    public function createImageStreamResponse($image) {
        imagepng($image);
        imagedestroy($image);
    }

    public function createDefaultImageResponse() {
        return response()->download(base_path('/resources/images/default.jpg'));
    }

    public function createPagedResponse($data) {
        $items = array();
        foreach ($data as $item) {
            array_push($items, $item);
        }

        return response()->json($items)
                        ->header('X-RecordsCount', $data->total());
    }

    public function createDefaultResponse($data = null) {
        return response()->json($data, 200);
    }

    public function createCreatedResponse($data) {
        return response()->json($data, 201);
    }

    public function createEmptyResponse() {
        return response()->make('', 204);
    }

    public function createBadRequestResponse($data = null) {
        return response()->json('poorly formed model data. =( '.$data, 400);
    }

    public function createModelBadRequestResponse($data = null) {
        return response()->json('model id property different from url id. =( '.$data, 400);
    }

    public function createPreConditionFailedResponse($data) {
        return response()->json($data, 412);
    }

    public function createInternalServerErrorResponse($data) {
        return response()->json($data, 500);
    }
}
