<?php

namespace App\Business;

use App\Repositories\CompanyResponsibleUserRepository as CompanyResponsibleUserRepository;
use App\Business\Contracts\BusinessInterface;
use App\Exceptions\EntityValidationException;
use App\Http\Helpers\Helper;

class CompanyResponsibleUserBusiness implements BusinessInterface {
    private $companyResponsibleUserRepository;

    public function __construct(CompanyResponsibleUserRepository $cru) {
        $this->companyResponsibleUserRepository = $cru;
    }

    public function paginate($pager) {
        return $this->companyResponsibleUserRepository->paginate($pager->currentPage, $pager->pageSize);
    }

    public function getById($id) {
        return $this->companyResponsibleUserRepository->getById($id);
    }

    public function save($entity) {
        $this->validate($entity, empty($entity->id));

        if (!empty($entity->id)) {
            $data = $this->getById($entity->id);
            if (empty($data))
                throw new EntityValidationException('invalid property id');
            $data->companyId = $entity->companyId;
            $data->userId = $entity->userId;

            return $this->companyResponsibleUserRepository->save($data);
        }

        return $this->companyResponsibleUserRepository->save($entity);
    }

    public function delete($id) {
        $this->companyResponsibleUserRepository->delete($id);
    }

    private function validate($entity, $new) {
        if (empty($entity->companyId))
            throw new EntityValidationException('companyId is required');
        
        if (empty($entity->userId))
            throw new EntityValidationException('userId is required');
        
        if ($new && !empty($this->companyResponsibleUserRepository->getByCompanyIdUserId($entity->companyId, $entity->userId)))
            throw new EntityValidationException("the user $entity->userId is already bound to company $entity->companyId");
    }

    public function getByCompanyIdUserId($companyId, $userId) {
        return $this->companyResponsibleUserRepository->getByCompanyIdUserId($companyId, $userId);
    }
}
