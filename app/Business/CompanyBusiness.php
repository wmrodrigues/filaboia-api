<?php

namespace App\Business;

use App\Repositories\CompanyRepository as CompanyRepository;
use App\Business\Contracts\BusinessInterface;
use App\Exceptions\EntityValidationException;
use App\Http\Helpers\Helper;
use App\Business\BranchBusiness as BranchBusiness;
use App\Business\CompanyResponsibleUserBusiness as CompanyResponsibleUserBusiness;
use App\Models\Branch as Branch;
use App\Business\UserBusiness as UserBusiness;
use App\Models\CompanyResponsibleUser as CompanyResponsibleUser;

class CompanyBusiness implements BusinessInterface {
    private $companyRepository;
    private $branchBusiness;
    private $userBusiness;
    private $companyResponsibleUserBusiness;

    public function __construct(CompanyRepository $cr, BranchBusiness $bb, UserBusiness $ub, CompanyResponsibleUserBusiness $crub) {
        $this->companyRepository = $cr;
        $this->branchBusiness = $bb;
        $this->userBusiness = $ub;
        $this->companyResponsibleUserBusiness = $crub;
    }

    public function paginate($pager) {
        return $this->companyRepository->paginate($pager->currentPage, $pager->pageSize);
    }

    public function getById($id) {
        return $this->companyRepository->getById($id);
    }

    public function save($entity) {
        $this->validate($entity, empty($entity->id));

        if (!empty($entity->id)) {
            $data = $this->getById($entity->id);
            if (empty($data))
                throw new EntityValidationException('invalid property id');
            $data->fantasyName = $entity->fantasyName;
            $data->officialName = $entity->officialName;
            $data->document = $entity->document;

            return $this->companyRepository->save($data);
        }

        $this->companyRepository->save($entity);
        if (empty($this->branchBusiness->getBranchCountByCompanyId($entity->id))) {
            $branch = new Branch();
            $branch->companyId = $entity->id;
            $branch->document = $entity->document;
            $branch->name = $entity->fantasyName;
            $this->branchBusiness->save($branch);
        }
    }

    public function delete($id) {
        $this->companyRepository->delete($id);
    }

    private function validate($entity, $new) {
        if (empty($entity->fantasyName))
            throw new EntityValidationException('fantasyName is required');
        
        if (empty($entity->officialName))
            throw new EntityValidationException('officialName is required');
        
        if (empty($entity->document))
            throw new EntityValidationException('document is required');
        
        if ($new && !empty($this->getByDocument($entity->document)))
            throw new EntityValidationException("there is already a company with the document $entity->document");
    }

    public function getByDocument($document) {
        return $this->companyRepository->getByDocument($document);
    }

    public function addResponsibleUsers($companyId, $users, $notFound = array()) {
        if (empty($users))
            throw new EntityValidationException('users list must have at least one item');
        
        $company = $this->getById($companyId);
        if (empty($company))
            throw new EntityValidationException('companyId not found');
        
        foreach ($users as $user) {
            $tmp = $this->userBusiness->getById($user);
            if (!empty($tmp)) {
                $exists = $this->companyResponsibleUserBusiness->getByCompanyIdUserId($companyId, $user);
                if (empty($exists)) {
                    $item = new CompanyResponsibleUser();
                    $item->companyId = $companyId;
                    $item->userId = $user;
                    $this->companyResponsibleUserBusiness->save($item);
                }
            } else {
                array_push($notFound, $user);
            }
        }
    }

    public function removeResponsibleUsers($companyId, $users) {
        if (empty($users))
            throw new EntityValidationException('users list must have at least one item');
        
        $company = $this->getById($companyId);
        if (empty($company))
            throw new EntityValidationException('companyId not found');
        
        foreach ($users as $user) {
            $tmp = $this->companyResponsibleUserBusiness->getByCompanyIdUserId($companyId, $user);
            if (!empty($tmp))
                $this->companyResponsibleUserBusiness->delete($tmp->id);
        }
    }
}