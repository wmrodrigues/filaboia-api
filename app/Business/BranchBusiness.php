<?php

namespace App\Business;

use App\Repositories\BranchRepository as BranchRepository;
use App\Business\Contracts\BusinessInterface;
use App\Exceptions\EntityValidationException;
use App\Http\Helpers\Helper;
use App\Models\BranchResponsibleUser as BranchResponsibleUser;
use App\Business\BranchResponsibleUserBusiness as BranchResponsibleUserBusiness;
use App\Business\UserBusiness as UserBusiness;

class BranchBusiness implements BusinessInterface {
    private $branchRepository;
    private $branchResponsibleUserBusiness;
    private $userBusiness;

    public function __construct(BranchRepository $br, BranchResponsibleUserBusiness $brub, UserBusiness $ub) {
        $this->branchRepository = $br;
        $this->branchResponsibleUserBusiness = $brub;
        $this->userBusiness = $ub;
    }

    public function paginate($pager) {
        return $this->branchRepository->paginate($pager->currentPage, $pager->pageSize);
    }

    public function getById($id) {
        return $this->branchRepository->getById($id);
    }

    public function save($entity) {
        $this->validate($entity, empty($entity->id));

        if (!empty($entity->id)) {
            $data = $this->getById($entity->id);
            if (empty($data))
                throw new EntityValidationException('invalid property id');
            $data->companyId = $entity->companyId;
            $data->document = $entity->document;
            $data->name = $entity->name;

            return $this->branchRepository->save($data);
        }

        return $this->branchRepository->save($entity);
    }

    public function delete($id) {
        $this->branchRepository->delete($id);
    }

    private function validate($entity, $new) {
        if (empty($entity->companyId))
            throw new EntityValidationException('companyId is required');
        
        if (empty($entity->document))
            throw new EntityValidationException('document is required');
        
        if (empty($entity->name))
            throw new EntityValidationException('name is required');
        
        if ($new && !empty($this->getByDocument($entity->document)))
            throw new EntityValidationException("there is already a branch with the document $entity->document");
        
        if ($new && !empty($this->getByCompanyIdName($entity->companyId, $entity->name)))
            throw new EntityValidationException("there is already a branch with the name $entity->name in this company");
    }

    public function getByDocument($document) {
        return $this->branchRepository->getByDocument($document);
    }

    public function getByCompanyIdName($companyId, $name) {
        return $this->branchRepository->getByCompanyIdName($companyId, $name);
    }

    public function getBranchCountByCompanyId($companyId) {
        return $this->branchRepository->getBranchCountByCompanyId($companyId);
    }

    public function getByCompanyId($companyId) {
        return $this->branchRepository->getByCompanyId($companyId);
    }

    public function addResponsibleUsers($branchId, $users, $notFound = array()) {
        if (empty($users))
            throw new EntityValidationException('users list must have at least one item');
        
        $branch = $this->getById($branchId);
        if (empty($branch))
            throw new EntityValidationException('branchId not found');
        
        foreach ($users as $user) {
            $tmp = $this->userBusiness->getById($user);
            if (!empty($tmp)) {
                $exists = $this->branchResponsibleUserBusiness->getByBranchIdUserId($branchId, $user);
                if (empty($exists)) {
                    $item = new BranchResponsibleUser();
                    $item->companyId = $branch->companyId;
                    $item->branchId = $branchId;
                    $item->userId = $user;
                    $this->branchResponsibleUserBusiness->save($item);
                }
            } else {
                array_push($notFound, $user);
            }
        }
    }

    public function removeResponsibleUsers($branchId, $users) {
        if (empty($users))
            throw new EntityValidationException('users list must have at least one item');
        
        $branch = $this->getById($branchId);
        if (empty($branch))
            throw new EntityValidationException('branchId not found');
        
        foreach ($users as $user) {
            $tmp = $this->branchResponsibleUserBusiness->getByBranchIdUserId($branchId, $user);
            if (!empty($tmp))
                $this->branchResponsibleUserBusiness->delete($tmp->id);
        }
    }
}