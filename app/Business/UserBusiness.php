<?php

namespace App\Business;

use App\Repositories\UserRepository as UserRepository;
use App\Business\Contracts\BusinessInterface;
use App\Exceptions\EntityValidationException;
use App\Http\Helpers\Helper;

class UserBusiness implements BusinessInterface {
    private $userRepository;

    public function __construct(UserRepository $ur) {
        $this->userRepository = $ur;
    }

    private function removePasswordField($data) {
        $data->each(function($item, $key) {
                unset($item->password);
                unset($item->remember_token);
            });
        return $data;
    }

    public function paginate($pager) {
        $data = $this->userRepository->paginate($pager->currentPage, $pager->pageSize);
        return $this->removePasswordField($data);
    }

    public function getById($id) {
        return $this->userRepository->getById($id);
    }

    public function save($entity) {
        $this->validate($entity, empty($entity->id));

        if (!empty($entity->id)) {
            $data = $this->getById($entity->id);
            if (empty($data))
                throw new EntityValidationException('invalid property id');
            $data->name = $entity->name;
            $data->login = $entity->login;
            $data->password = $entity->password;
            $data->document = $entity->document;
            $data->phone = $entity->phone;
            $data->email = $entity->email;

            return $this->userRepository->save($data);
        }

        return $this->userRepository->save($entity);
    }

    public function delete($id) {
        $this->userRepository->delete($id);
    }

    private function validate($entity, $new) {
        if (empty($entity->name))
            throw new EntityValidationException('name is required');
        
        if (empty($entity->login))
            throw new EntityValidationException('login is required');
        
        if (empty($entity->document))
            throw new EntityValidationException('document is required');
        
        if (empty($entity->birthDate))
            throw new EntityValidationException('birhtDate is required');
        
        if ($new && !empty($this->userRepository->getByLogin($entity->login)))
            throw new EntityValidationException("there is already an user with the login $entity->login");
        
        if ($new && !empty($this->userRepository->getByDocument($entity->document)))
            throw new EntityValidationException("there is already an user with the document $entity->document");
    }

    public function getByLoginPassword($login, $password) {
        if (empty($login))
            throw new EntityValidationException('login is required');
        
        if (empty($password))
            throw new EntityValidationException('password is required');
        
        return $this->userRepository->getByLoginPassword($login, $password);
    }

    public function getByToken($token) {
        if (empty($token))
            throw new EntityValidationException('token is required');
        
        return $this->userRepository->getByToken($token);
    }

    public function getResponsibleUsersByCompanyId($companyId) {
        if (empty($companyId))
            throw new EntityValidationException('companyId is required');
        
        $data = $this->userRepository->getResponsibleUsersByCompanyId($companyId);
        return $this->removePasswordField($data);
    }

    public function getResponsibleUsersByBranchId($branchId) {
        if (empty($branchId))
            throw new EntityValidationException('branchId is required');
        
        $data = $this->userRepository->getResponsibleUsersByBranchId($branchId);
        return $this->removePasswordField($data);
    }
}