<?php

namespace App\Business;

use App\Repositories\BranchResponsibleUserRepository as BranchResponsibleUserRepository;
use App\Business\Contracts\BusinessInterface;
use App\Exceptions\EntityValidationException;
use App\Http\Helpers\Helper;

class BranchResponsibleUserBusiness implements BusinessInterface {
    private $branchResponsibleUserRepository;

    public function __construct(BranchResponsibleUserRepository $bru) {
        $this->branchResponsibleUserRepository = $bru;
    }

    public function paginate($pager) {
        return $this->branchResponsibleUserRepository->paginate($pager->currentPage, $pager->pageSize);
    }

    public function getById($id) {
        return $this->branchResponsibleUserRepository->getById($id);
    }

    public function save($entity) {
        $this->validate($entity, empty($entity->id));

        if (!empty($entity->id)) {
            $data = $this->getById($entity->id);
            if (empty($data))
                throw new EntityValidationException('invalid property id');
            $data->companyId = $entity->companyId;
            $data->branchId = $entity->branchId;
            $data->userId = $entity->userId;

            return $this->branchResponsibleUserRepository->save($data);
        }

        return $this->branchResponsibleUserRepository->save($entity);
    }

    public function delete($id) {
        $this->branchResponsibleUserRepository->delete($id);
    }

    private function validate($entity, $new) {
        if (empty($entity->companyId))
            throw new EntityValidationException('companyId is required');
        
        if (empty($entity->branchId))
            throw new EntityValidationException('branchId is required');

        if (empty($entity->userId))
            throw new EntityValidationException('userId is required');
        
        if ($new && !empty($this->branchResponsibleUserRepository->getByCompanyIdBranchIdUserId($entity->companyId, $entity->branchId, $entity->userId)))
            throw new EntityValidationException("the user $entity->userId is already bound to company $entity->companyId and branch $entity->branchId");
    }

    public function getByBranchIdUserId($companyId, $userId) {
        return $this->branchResponsibleUserRepository->getByBranchIdUserId($companyId, $userId);
    }
}
