<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSchema extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('company', function(BluePrint $table) {
            $table->increments('id');
            $table->timestamps();
            $table->string('fantasyName', 50);
            $table->string('officialName', 50);
            $table->string('document', 20);
        });

        Schema::create('branch', function(BluePrint $table) {
            $table->increments('id');
            $table->timestamps();
            $table->integer('companyId');
            $table->string('document', 20);
            $table->string('name', 50);

            $table->foreign('companyId')->references('id')->on('company');
        });

        Schema::create('user', function(BluePrint $table) {
            $table->increments('id');
            $table->timestamps();
            $table->string('name', 200);
            $table->string('login', 100);
            $table->string('password', 500);
            $table->string('document', 20);
            $table->date('birthDate');
            $table->string('phone', 20)->nullable();
            $table->string('email', 100)->nullable();
            $table->string('remember_token', 100)->nullable();
        });

        Schema::create('companyresponsibleuser', function(BluePrint $table) {
            $table->increments('id');
            $table->timestamps();
            $table->integer('companyId');
            $table->integer('userId');

            $table->foreign('companyId')->references('id')->on('company');
            $table->foreign('userId')->references('id')->on('user');
        });

        Schema::create('branchresponsibleuser', function(BluePrint $table) {
            $table->increments('id');
            $table->timestamps();
            $table->integer('companyId');
            $table->integer('branchId');
            $table->integer('userId');

            $table->foreign('companyId')->references('id')->on('company');
            $table->foreign('branchId')->references('id')->on('branch');
            $table->foreign('userId')->references('id')->on('user');
        });

        Schema::create('item', function(BluePrint $table) {
            $table->increments('id');
            $table->timestamps();
            $table->string('name', 20);
            $table->decimal('price', 10,2);
            $table->integer('parentitemId')->nullable();
        });

        Schema::create('itemcategory', function(BluePrint $table) {
            $table->increments('id');
            $table->timestamps();
            $table->string('name', 50);
        });

        Schema::create('menu', function(BluePrint $table) {
            $table->increments('id');
            $table->timestamps();
            $table->string('name');
            $table->integer('companyId');
            $table->integer('branchId')->nullable();

            $table->foreign('companyId')->references('id')->on('company');
        });

        Schema::create('menuitem', function(BluePrint $table) {
            $table->increments('id');
            $table->timestamps();
            $table->integer('menuId');
            $table->integer('itemId');
            $table->decimal('price', 10,2);

            $table->foreign('menuId')->references('id')->on('menu');
            $table->foreign('itemId')->references('id')->on('item');
        });

        Schema::create('purchase', function(BluePrint $table) {
            $table->increments('id');
            $table->timestamps();
            $table->date('orderDate');
            $table->decimal('sumValue', 10,2);
            $table->decimal('discount', 10,2);
            $table->decimal('total', 20,2);
            $table->integer('companyId');
            $table->integer('branchId');
            $table->integer('userId');

            $table->foreign('companyId')->references('id')->on('company');
            $table->foreign('branchId')->references('id')->on('branch');
            $table->foreign('userId')->references('id')->on('user');
        });

        Schema::create('purchaseitem', function(BluePrint $table){
            $table->increments('id');
            $table->timestamps();
            $table->integer('purchaseId');
            $table->integer('itemId');
            $table->decimal('price', 10,2);
            $table->decimal('discount', 20,2);

            $table->foreign('purchaseId')->references('id')->on('purchase');
            $table->foreign('itemId')->references('id')->on('item');
        });

        Schema::create('removepurchaseitem', function(BluePrint $table) {
            $table->increments('id');
            $table->timestamps();
            $table->integer('purchaseId'); //pedido
            $table->integer('purchaseItemId'); //item do pedido
            $table->integer('itemId'); //item a ser removido

            $table->foreign('purchaseId')->references('id')->on('purchase');
            $table->foreign('purchaseItemId')->references('id')->on('purchaseitem');
            $table->foreign('itemId')->references('id')->on('item');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('company');
        Schema::dropIfExists('branch');
        Schema::dropIfExists('user');
        Schema::dropIfExists('companyresponsibleuser');
        Schema::dropIfExists('branchresponsibleuser');
        Schema::dropIfExists('item');
        Schema::dropIfExists('itemcategory');
        Schema::dropIfExists('menu');
        Schema::dropIfExists('menuitem');
        Schema::dropIfExists('purchase');
        Schema::dropIfExists('purchaseitem');
        Schema::dropIfExists('removepurchaseitem');
    }
}
