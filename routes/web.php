<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$app->get('/', function () use ($app) {
    return $app->version();
});

$app->group(['prefix' => 'api/v1'], function($app) {
    $app->get('companys', ['middleware' => 'auth', 'uses' => 'CompanysController@get']);
    $app->get('companys/{id}', 'CompanysController@getById');
    $app->post('companys', 'CompanysController@post');
    $app->put('companys/{id}', 'CompanysController@put');
    $app->delete('companys/{id}', 'CompanysController@delete');

    $app->get('branchs', 'BranchsController@get');
    $app->get('branchs/{id}', 'BranchsController@getById');
    $app->post('branchs', 'BranchsController@post');
    $app->put('branchs/{id}', 'BranchsController@put');
    $app->delete('branchs/{id}', 'BranchsController@delete');
    $app->get('companys/{id}/branchs', 'BranchsController@getByCompanyId');

    $app->get('users', 'UsersController@get');
    $app->get('users/{id}', 'UsersController@getById');
    $app->post('users', 'UsersController@post');
    $app->put('users/{id}', 'UsersController@put');
    $app->delete('users/{id}', 'UsersController@delete');
    $app->post('companys/{id}/users', 'UsersController@postCompanyResponsibleUsers');
    $app->delete('companys/{id}/users', 'UsersController@deleteCompanyResponsibleUsers');
    $app->get('companys/{id}/users', 'UsersController@getResponsibleUsersByCompanyId');
    $app->post('branchs/{id}/users', 'UsersController@postBranchResponsibleUsers');
    $app->delete('branchs/{id}/users', 'UsersController@deleteBranchResponsibleUsers');
    $app->get('branchs/{id}/users', 'UsersController@getResponsibleUsersByBranchId');

    $app->post('accounts', 'AccountsController@postLoginToken');
    $app->post('tokens', 'AccountsController@postToken');
    $app->post('token', 'AccountsController@requestToken');
    $app->post('auth', 'AccountsController@validateToken');
});